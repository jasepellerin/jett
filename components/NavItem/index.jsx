import InternalLinkButton from '../InternalLinkButton'
import styles from './NavItem.scss'

const NavItem = ({ isActive, title, ...rest }) => (
  <InternalLinkButton className={isActive ? styles.active : null} styles={styles} {...rest}>
    {title}
  </InternalLinkButton>
)

export default NavItem
