import { useState, useEffect } from 'react'
import Div from '../Base/Div'
import FlexContainer from '../FlexContainer'
import CarouselPage from '../CarouselPage'
import CarouselButton from './components/CarouselButton'
import styles from './Carousel.scss'

const Carousel = ({
  sections,
  title,
  ...rest
}) => {
  const time = 5000
  let intervalContainer
  const [currentIndex, setCurrentIndex] = useState(0)
  const setIndexInterval = () => {
    clearInterval(intervalContainer)
    intervalContainer = setInterval(() => {
      setCurrentIndex((currentIndex + 1) % sections.length)
    }, time)
  }

  const setCurrentIndexHandler = (index) => {
    setCurrentIndex(index)
    setIndexInterval()
  }

  useEffect(() => {
    setIndexInterval()
    return () => {
      clearInterval(intervalContainer)
    }
  })

  return (
    <Div
      styles={styles}
      {...rest}
    >
      <h3><em>{title}</em></h3>
      <CarouselPage
        key={currentIndex}
        content={sections[currentIndex]}
      />
      <FlexContainer className='quarter-width'>
        {sections.map((section, index) => (
          <CarouselButton
            index={index}
            isActive={index === currentIndex}
            key={index}
            setCurrentIndex={setCurrentIndexHandler}
          />
        ))}
      </FlexContainer>
    </Div>
  )
}

export default Carousel
