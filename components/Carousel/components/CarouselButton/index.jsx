import { memo } from 'react'
import classNames from 'classnames'
import Button from '../../../Button'

const CarouselButton = memo(({
  index,
  isActive,
  setCurrentIndex,
  ...rest
}) => (
  <Button
    variation={classNames('diamond', { selected: isActive })}
    onClick={() => !isActive && setCurrentIndex(index)}
    title={`Select Quote ${index + 1}`}
    {...rest}
  >
  </Button>
))

export default CarouselButton
