import styles from './ContentSection.scss'
import Div from '../Base/Div'

const ContentSection = ({
  children,
  notchLocation = 'none',
  theme = 'light',
  variation,
  ...rest
}) => (
  <Div
    variation={`${variation} notch-${notchLocation} ${theme}`}
    styles={styles}
    {...rest}
  >
    {children}
  </Div>
)

export default ContentSection
