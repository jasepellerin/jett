import styles from './Logo.scss'
import InternalLinkButton from '../InternalLinkButton'
import Image from '../Base/Image'

const Logo = ({ ...rest }) => (
  <InternalLinkButton href='/' styles={styles}>
    <Image src='../../static/logo.png' />
  </InternalLinkButton>
)

export default Logo
