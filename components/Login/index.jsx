import { useState } from 'react'
import Div from '../Base/Div'
import Button from '../Button'
import styles from './Login.scss'
import FlexContainer from '../FlexContainer'

const password = 'jettAccess19'

const Login = ({ successCallback }) => {
  const [value, setValue] = useState('')
  const [error, setError] = useState('')

  const updateValue = ({ target: { value } }) => {
    setValue(value)
  }

  const onSubmit = () => {
    if (value === password) {
      successCallback()
    } else {
      setError('Invalid password, please try again')
    }
  }

  return (
    <FlexContainer variation='column'>
      {error && (
        <Div className={styles.errorContainer}>{error}</Div>
      )}
      <input className={styles.login} type='password' name='login' onChange={updateValue} placeholder='Enter Password'></input>
      <Button onClick={onSubmit} variation="transparent-highlight">Submit</Button>
    </FlexContainer>
  )
}

export default Login
