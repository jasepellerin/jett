import Link from 'next/link'
import Button from '../Button'

const InternalLinkButton = ({
  href,
  ...rest
}) => (
  <Link href={href}>
    <Button {...rest} />
  </Link>
)

export default InternalLinkButton
