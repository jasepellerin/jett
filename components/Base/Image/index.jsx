import BaseComponent from '../../Base/BaseComponent'

const Image = ({ ...rest }) => (
  <BaseComponent element='img' {...rest} />
)

export default Image
