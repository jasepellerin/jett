import BaseComponent from '../../Base/BaseComponent'

const Div = ({ ...rest }) => (
  <BaseComponent element='div' {...rest} />
)

export default Div
