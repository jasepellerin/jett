import classNames from 'classnames'

const BaseComponent = ({
  children,
  className = '',
  element,
  variation = '',
  styles = {},
  ...rest
}) => {
  const Element = element
  const styleNames = variation.split(' ')
  const validStyles = styleNames.filter(name => {
    return styles.hasOwnProperty(name)
  })
    .map(name => styles[name])
    .join(' ') ||
    styles['base']

  return (
    <Element className={classNames(className, validStyles)} {...rest}>
      {children}
    </Element>
  )
}

export default BaseComponent
