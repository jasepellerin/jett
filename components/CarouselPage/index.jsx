import Div from '../Base/Div'
import styles from './CarouselPage.scss'

const CarouselPage = ({
  content: {
    body,
    footnote
  },
  ...rest
}) => (
  <Div styles={styles} {...rest}>
    <p><q>{body}</q></p>
    <div><em>{footnote}</em></div>
  </Div>
)

export default CarouselPage
