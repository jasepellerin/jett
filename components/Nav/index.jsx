import BaseComponent from '../Base/BaseComponent'
import navItems from '../../content/nav.json'
import NavItem from '../NavItem'
import styles from './Nav.scss'

const navItemComponents = (currentPage) => navItems.map(({ ...itemProps }) => (
  <NavItem isActive={currentPage === itemProps.href} key={itemProps.title} {...itemProps} />
))

const Nav = ({ currentPage }) => (
  <BaseComponent element='nav' styles={styles}>
    {navItemComponents(currentPage)}
  </BaseComponent>
)

export default Nav
