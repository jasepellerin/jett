import styles from './Hexagon.scss'
import Div from '../Base/Div'

const Hexagon = ({ children, ...rest }) => (
  <Div styles={styles} {...rest}>
    {children}
  </Div>
)

export default Hexagon
