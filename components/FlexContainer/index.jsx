import Div from '../Base/Div'
import styles from './FlexContainer.scss'

const FlexContainer = ({ ...rest }) => (
  <Div element='div' styles={styles} {...rest} />
)

export default FlexContainer
