import Head from 'next/head'

const CustomHead = ({ title }) => (
  <Head>
    <title>{title}</title>
    <meta name='viewport' content='initial-scale=1, width=device-width, maximum-scale=1, user-scalable=no' />
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,600,700' rel='stylesheet' />
    <link rel='shortcut icon' href='static/favicon.ico' type='image/x-icon' />
    <link rel='icon' href='static/favicon.ico' type='image/x-icon' />
  </Head>
)

export default CustomHead
