import BaseComponent from '../Base/BaseComponent'
import styles from './Button.scss'

const Button = ({
  children,
  onClick,
  ...rest
}) => (
  <BaseComponent
    element='button'
    styles={styles}
    onClick={onClick}
    {...rest}
  >
    {children}
  </BaseComponent>
)

export default Button
