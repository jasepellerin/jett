import ServicesModule from '../modules/Services'

const Services = () => (
  <ServicesModule />
)

export default Services
