import AboutModule from '../modules/About'

const About = () => (
  <AboutModule />
)

export default About
