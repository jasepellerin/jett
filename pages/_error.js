import { Component } from 'react'
import ContentSection from '../components/ContentSection'
import InternalLinkButton from '../components/InternalLinkButton'

export default class Error extends Component {
  static getInitialProps ({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null
    return { statusCode }
  }

  getErrorResponse (statusCode = null) {
    switch (statusCode) {
      case null: return 'Sorry, an error has occurred'
      case 404: return 'Sorry, we can\'t find that page'
      default: return `Sorry, error ${statusCode} occurred on server`
    }
  }

  render () {
    const { statusCode } = this.props
    return (
      <ContentSection>
        <h3>
          {
            this.getErrorResponse(statusCode)
          }
        </h3>
        <InternalLinkButton href={'/'} variation='transparent-highlight'>
          Go Home
        </InternalLinkButton>
      </ContentSection>
    )
  }
}
