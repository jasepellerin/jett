import React from 'react'
import App, { Container } from 'next/app'
import CustomHead from '../components/CustomHead'
import FlexContainer from '../components/FlexContainer'
import Footer from '../modules/Footer'
import Logo from '../components/Logo'
import Nav from '../components/Nav'

import '../styles/global/_global.scss'

class CustomApp extends App {
  static async getInitialProps ({ Component, router, ctx }) {
    let pageProps = {}

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }

    return { pageProps }
  }

  render () {
    const { Component, pageProps, router } = this.props

    return (
      <Container>
        <CustomHead title='Jett Solutions, LLC' />
        <FlexContainer variation='app'>
          <FlexContainer className='full-width'>
            <Logo />
            <Nav currentPage={router.route} />
          </FlexContainer>
          <Component {...pageProps} />
          <Footer currentPage={router.route} />
        </FlexContainer>
      </Container>
    )
  }
}

export default CustomApp
