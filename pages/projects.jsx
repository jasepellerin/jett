import ProjectsModule from '../modules/Projects'

const Projects = () => (
  <ProjectsModule />
)

export default Projects
