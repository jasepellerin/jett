const sass = require('@zeit/next-sass')
const withPlugins = require('next-compose-plugins')
// const optimizeImages = require('next-optimized-images')

module.exports = withPlugins([
  [sass, {
    cssModules: true,
    cssLoaderOptions: {
      importLoaders: 1,
      localIdentName: '[folder]-[local]___[hash:base64:5]'
    }
  }]
  // [optimizeImages]
])
