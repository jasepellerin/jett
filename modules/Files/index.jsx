import { useEffect, useState } from 'react'
import Div from '../../components/Base/Div'
import Login from '../../components/Login'
import cookieManager from '../../utils/cookieManager'

const Files = () => {
  const [loggedIn, setLoggedIn] = useState(null)
  const handleLogin = () => {
    cookieManager.setCookie()
    setLoggedIn(true)
  }

  const getContents = () => {
    switch (loggedIn) {
      case true: return 'No files are available at this time, please check back later.'
      case false: return <Login successCallback={handleLogin} />
      default: return null
    }
  }

  useEffect(() => {
    const cookie = cookieManager.getCookie()
    if (cookie === 'valid') {
      setLoggedIn(true)
    } else {
      setLoggedIn(false)
    }
  })

  return (
    <Div>
      {getContents()}
    </Div>
  )
}

export default Files
