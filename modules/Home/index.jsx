import History from './History'
import Sections from './Sections'
import HomeSplash from './Splash'
import Testimonials from './Testimonials'
import Contact from './Contact'

const Home = () => (
  <>
    <HomeSplash />
    <History />
    <Sections />
    {/* <Testimonials /> */}
    <Contact />
  </>
)

export default Home
