import ContentSection from '../../../components/ContentSection'
import Section from './components/Section'

const sections = [
  {
    text: 'We offer a wide range of solutions including Electrical, Gas Detection Systems, Electrical Panel Construction, Energy Management Systems, Thermo-graphics (infrared predictive maintenance scans), and Telecom. To find more information regarding all of our available services, click the link below.',
    title: 'Our Services',
    link: {
      title: 'Services',
      href: '/services'
    }
  },
  {
    text: 'Here at Jett Solutions, LLC, we take pride in our numerous jobs completed with quality and efficiency. It is important to us that we give our customers the best experience from start to finish. Take a look at some of our finished work and see for yourself the attention to detail we strive for.',
    title: 'Projects',
    link: {
      title: 'Projects',
      href: '/projects'
    }
  },
  {
    text: 'Jett Solutions is a service oriented, woman owned company based in Duncan, OK. We pride ourselves in our ethics and being a company based on Christian values. We started as an Electrical and Energy Management company for a few large corporations, but we have grown and diversified through the years since. To hear more about our history and values, continue below.',
    title: 'About Us',
    link: {
      title: 'About',
      href: '/about'
    }
  }
]

const Sections = () => (
  <ContentSection notchLocation='top-center' theme='dark column'>
    {sections.map(Section)}
  </ContentSection>
)

export default Sections
