import InternalLinkButton from '../../../../components/InternalLinkButton'
import Div from '../../../../components/Base/Div'

const Section = ({ text, title, link }, index) => (
  <Div key={index}>
    <Div>
      <h3><em>{title}</em></h3>
      <p>{text}</p>
    </Div>
    <InternalLinkButton {...link}>{link.title}</InternalLinkButton>
  </Div>
)

export default Section
