import ContentSection from '../../../components/ContentSection'
import Carousel from '../../../components/Carousel'

const sections = [
  {
    body: 'Jett Solutions are the best! I would recommend them for everything from your electrical needs to gas monitoring! 1',
    footnote: '- A Happy Client'
  },
  {
    body: 'Jett Solutions are the best! I would recommend them for everything from your electrical needs to gas monitoring! 2',
    footnote: '- A Happy Client'
  },
  {
    body: 'Jett Solutions are the best! I would recommend them for everything from your electrical needs to gas monitoring! 3',
    footnote: '- A Happy Client'
  }
]

const Testimonials = () => (
  <ContentSection theme='carousel industrial' notchLocation='bottom-right'>
    <Carousel sections={sections} title='What our clients are saying.' />
  </ContentSection>
)

export default Testimonials
