import ContentSection from '../../../components/ContentSection'
import InternalLinkButton from '../../../components/InternalLinkButton'

const Contact = () => (
  <ContentSection theme='light'>
    <h3><em>Ready to talk about your project?</em></h3>
    <InternalLinkButton href='/contact' variation="large transparent-highlight">
      Get in touch
    </InternalLinkButton>
  </ContentSection>
)

export default Contact
