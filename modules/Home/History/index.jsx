import ContentSection from '../../../components/ContentSection'
import Div from '../../../components/Base/Div'
import Image from '../../../components/Base/Image'

const History = () => (
  <ContentSection theme='light image-right'>
    <Div>
      <h3><em>Based in Duncan, Oklahoma...</em></h3>
      <p>
        Jett Solutions, LLC provides a diverse set of services with professionalism, attention to detail, and upmost efficiency. Jett Solutions is your trusted source of licensed experts available to take on your electrical projects from Medium Voltage switch gear, Commercial and Industrial switch gear installs, retro fits and upgrades, to a long list of control panels designed and built in our listed panel shop. Our experts are poised to help accomplish your specific requirements.
      </p>
    </Div>
    <Image src='../../../static/home/outline.jpg' />
  </ContentSection>
)

export default History
