import Splash from '../../Shared/Splash'

const HomeSplash = () => (
  <Splash
    backgroundImage='/static/home/splash.jpg'
    notchLocation='bottom-right'
  >
    <h1>Jett Solutions</h1>
    <p>
      <em>Aspiring to a <b>Higher</b> Level</em>
    </p>
  </Splash>
)

export default HomeSplash
