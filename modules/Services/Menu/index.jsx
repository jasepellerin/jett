import BaseComponent from '../../../components/Base/BaseComponent'
import styles from './ServicesMenu.scss'
import InternalLinkButton from '../../../components/InternalLinkButton'

const ServicesMenu = ({ services }) => (
  <BaseComponent element='nav' styles={styles}>
    {services.map(({ title }) => (
      <InternalLinkButton
        href={`#${title.split(' ').join('_')}`}
        key={title}
      >
        {title}
      </InternalLinkButton>
    ))}
  </BaseComponent>
)

export default ServicesMenu
