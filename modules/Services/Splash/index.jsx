import Splash from '../../Shared/Splash'

const ServicesSplash = () => (
  <Splash
    backgroundImage='/static/services/splash.jpg'
    notchLocation='none'
    style={{
      backgroundPosition: '50% 50%'
    }}
  >
    <h1>Our Services</h1>
    <p>
        What Can Jett Do For You?
    </p>
  </Splash>
)

export default ServicesSplash
