import ServicesSplash from './Splash'
import Service from './Service'
import services from './services.json'
import ServicesMenu from './Menu'

const Services = () => (
  <>
    <ServicesSplash />
    <ServicesMenu services={services}/>
    {
      services.map(({ content, src, title, toggleContent = null }, index) => (
        <Service
          content={content}
          key={title}
          id={title.split(' ').join('_')}
          last={index === services.length - 1 ? 'true' : null}
          src={src}
          theme={index % 2 ? 'dark' : 'light'}
          title={title}
          toggleContent={toggleContent}
          variation={index % 2 ? '' : 'left'}
        />
      ))
    }
  </>
)

export default Services
