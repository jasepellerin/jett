import { useState } from 'react'
import Button from '../../../components/Button'
import FlexContainer from '../../../components/FlexContainer'
import Image from '../../../components/Base/Image'
import Div from '../../../components/Base/Div'
import ContentSection from '../../../components/ContentSection'

const getNotchLocation = ({ theme, last }) => {
  if (theme === 'dark') {
    if (last) {
      return 'top-left'
    }
    return 'both'
  }
  return null
}

const Service = ({ content, src, title, toggleContent, variation, ...rest }) => {
  const isDark = rest.theme === 'dark'
  const [isOpen, setIsOpen] = useState(false)
  const toggleIsOpen = () => {
    setIsOpen(!isOpen)
  }
  const image = <Image src={src} style={{ maxWidth: '40%', alignSelf: 'center' }} />
  const button = <Button variation={isDark ? 'transparent' : 'transparent-highlight'} onClick={toggleIsOpen} style={{ marginTop: '3vh' }}>
    {isOpen ? 'Hide Info' : 'Learn More'}
  </Button>
  const contentSection = (
    <FlexContainer variation='column' style={{ width: '50%', padding: '2vh 0', whiteSpace: 'pre-line', justifyContent: 'center' }}>
      <h2>{title}</h2>
      {content}
      {toggleContent && button}
    </FlexContainer>
  )

  function renderBody () {
    return variation === 'left'
      ? (
        <>
          {image}
          {contentSection}
        </>
      )
      : (
        <>
          {contentSection}
          {image}
        </>
      )
  }

  return (
    <ContentSection variation='services' notchLocation={getNotchLocation(rest)} {...rest}>
      <FlexContainer>
        {renderBody()}
      </FlexContainer>
      {
        isOpen
          ? (
            <Div style={{ padding: '5vh 10vw', whiteSpace: 'pre-line' }}>
              {toggleContent}
            </Div>
          ) : null
      }
    </ContentSection>
  )
}

export default Service
