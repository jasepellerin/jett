import Service from '../../Services/Service'

const Project2Before = () => (
  <Service
    content='Switchgear lineup changeout at a Mississippi Distribution Center. This picture shows the pad after the old Switchgear had been removed. We had two simultaneous shutdowns in 2 different buildings going on with the removal of two large lineups, 4 large transformers and 3 separate smaller panels all at the same outage. Tear down, re-install, test, energize and cleanup was safely completed in 27 hours.'
    src='/static/projects/project2before.png'
    theme='dark'
    title='Project 2 - Before'
    variation='left'
  />
)

export default Project2Before
