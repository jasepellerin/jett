import Service from '../../Services/Service'

const Project2After = () => (
  <Service
    content='This is a picture of the above, (which was one of the Switchgear lineups), after we were complete.'
    src='/static/projects/project2after.png'
    title='Project 2 - After'
  />
)

export default Project2After
