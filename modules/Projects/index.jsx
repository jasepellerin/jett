import ProjectsSplash from './Splash'
import Project1 from './Project1'
import Project2Before from './Project2Before'
import Project2After from './Project2After'

const Projects = () => (
  <>
    <ProjectsSplash />
    <Project1 />
    <Project2Before />
    <Project2After />
  </>
)

export default Projects
