import Splash from '../../Shared/Splash'

const ProjectsSplash = () => (
  <Splash notchLocation='bottom-left' backgroundImage='/static/projects/splash.jpg'>
    <h1>Projects</h1>
    <p>
        Our Finest Work
    </p>
  </Splash>
)

export default ProjectsSplash
