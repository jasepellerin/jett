import Service from '../../Services/Service'

const Project1 = () => (
  <Service
    content='Medium Voltage Switchgear Changeout at a Texas Distributions Center. This changeout involved completely disassembling the existing deteriorating switchgear, replacing it with new state-of-the-art switchgear and cleaning up the jobsite, including trucking out the disassembled old gear. We were given a time frame to be down of 24 hrs. We were complete and off premise in 22 hrs.'
    src='/static/projects/project1.png'
    title='Project 1'
  />
)

export default Project1
