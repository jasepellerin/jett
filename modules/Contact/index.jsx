import ContactSplash from './Splash'
import Email from './Email'
import Hexagon from '../../components/Hexagon'
import Div from '../../components/Base/Div'

const Contact = () => (
  <>
    <ContactSplash />
    <Div style={{ position: 'relative' }}>
      <Hexagon>OR</Hexagon>
    </Div>
    <Email />
  </>
)

export default Contact
