import ContentSection from '../../../components/ContentSection'
import Div from '../../../components/Base/Div'
import Button from '../../../components/Button'
import styles from './Email.scss'
import sendEmail from './sendEmail'
import { useState } from 'react'

const Email = () => {
  const [formContents, setFormContents] = useState({})
  const [error, setError] = useState(null)
  const [complete, setComplete] = useState(false)
  const [sending, setSending] = useState(false)
  const verifyAndSendEmail = () => {
    if (sending) {
      return
    }
    setSending(true)
    if (!formContents.email || !formContents.message || !formContents.name) {
      const errorParts = `${formContents.name ? '' : 'name '}${formContents.email ? '' : 'email '}${formContents.message ? '' : 'message'}`.trim().split(' ')
      const errorPartsMessage = [
        errorParts.slice(0, -1).join(', '),
        errorParts.slice(-1)[0]
      ].join(errorParts.length < 3 ? ' and ' : ', and ')
      const errorMessage = `Please include ${errorPartsMessage}`
      setError(errorMessage)
      setSending(false)
    } else {
      sendEmail(formContents).then((response) => {
        if (response.data.status === 'success') {
          setComplete(true)
          setSending(false)
        } else {
          setError('Unable to send email, please try again in a few minutes.')
          setSending(false)
        }
      })
    }
  }
  const updateForm = ({ target }) => {
    const contents = { ...formContents }
    contents[target.name] = target.value
    setError(null)
    setFormContents(contents)
  }
  const form = (
    <form action='javascript:void(0);' >
      <Div>
        <label>
            Name*
          <input name='name' onChange={updateForm} type='text'></input>
        </label>
        <label >
            Email*
          <input name='email' onChange={updateForm} type='email'></input>
        </label>
        <label>
            Phone
          <input name='phone' onChange={updateForm}></input>
        </label>
      </Div>
      <label>
          Message*
        <textarea name='message' onChange={updateForm}></textarea>
      </label>
      <Button disabled={sending} onClick={verifyAndSendEmail} variation="transparent-highlight medium">Send</Button>
    </form>
  )
  return (
    <ContentSection theme='light' styles={styles}>
      <h2>Send Us a Message</h2>
      {error && <Div className={styles.errorContainer}><h3>{error}</h3></Div>}
      {complete ? 'Thank you for contacting us! We will get back to you shortly.' : form}
    </ContentSection>
  )
}

export default Email
