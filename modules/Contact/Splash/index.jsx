import SVG from 'react-inlinesvg'
import Splash from '../../Shared/Splash'
import Div from '../../../components/Base/Div'
import FlexContainer from '../../../components/FlexContainer'
import styles from './ContactSplash.scss'

const ContactSplash = () => (
  <Splash style={{ height: 'unset' }} backgroundImage='/static/contact/splash.jpg'>
    <Div styles={styles}>
      <h3>Contact Us</h3>
      <FlexContainer className={styles.full}>
        <Div>
          <a href="tel:580-251-9858">
            <SVG src={'/static/phone.svg'} />
            Office: (580) 251-9858
          </a>
          <br></br>
          <a href="tel:580-255-5705">
            Fax: (580) 255-5705
          </a>
        </Div>
        <Div>
          <a href="mailto:info@jettsolutionsllc.com">
            <SVG src={'/static/mail.svg'} />
            info@jettsolutionsllc.com
          </a>
        </Div>
        <Div>
          <a href="https://www.google.com/maps/place/7322+US-81,+Duncan,+OK+73533/@34.5844576,-97.9688109,17z/data=!3m1!4b1!4m5!3m4!1s0x87ad42b6c28b3259:0x18b0cbec2917741f!8m2!3d34.5844576!4d-97.9666222">
            <SVG src={'/static/pin.svg'} />
            7322 N. Hwy 81
            <br></br>
            Duncan, OK  73533
          </a>
        </Div>
      </FlexContainer>
    </Div>
  </Splash>
)

export default ContactSplash
