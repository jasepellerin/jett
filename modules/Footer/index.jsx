import Link from 'next/link'
import ContentSection from '../../components/ContentSection'
import FlexContainer from '../../components/FlexContainer'
import FooterContent from '../FooterContent'
import styles from './Footer.scss'
import Div from '../../components/Base/Div'

const Footer = ({ currentPage }) => {
  return (
    <ContentSection notchLocation={currentPage === '/services' ? null : 'top-left'} theme='extraDark footer'>
      <FlexContainer className='full-width' styles={styles}>
        <FooterContent />
        <h3><em>Aspiring to a Higher Level</em></h3>
        <Div>© 2019 Jett Solutions, LLC | <Link href='/files'><a>Login</a></Link></Div>
      </FlexContainer>
    </ContentSection>
  )
}

export default Footer
