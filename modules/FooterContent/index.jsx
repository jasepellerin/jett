import Div from '../../components/Base/Div'
import Image from '../../components/Base/Image'
import styles from './FooterContent.scss'
import InternalLinkButton from '../../components/InternalLinkButton'

const FooterContent = () => {
  return (
    <Div styles={styles}>
      <Div>
        <InternalLinkButton href="/" variation="imageButton">
          <Image src="../../static/logo.png" className={styles.logo} />
        </InternalLinkButton>
      </Div>
      <Div className={styles.contact}>
        <p>
          {'Jett Solutions, LLC'}
          <a href="tel:580-255-5705">
            {`
          (580) 251-9858`}
          </a>
          <a href="https://www.google.com/maps/place/7322+US-81,+Duncan,+OK+73533/@34.5844576,-97.9688109,17z/data=!3m1!4b1!4m5!3m4!1s0x87ad42b6c28b3259:0x18b0cbec2917741f!8m2!3d34.5844576!4d-97.9666222">
            {`
          7322 N. Hwy 81
          Duncan, OK  73533`}
          </a>
        </p>
      </Div>
      <Div className={styles.bbbLink}>
        <a
          href="https://www.bbb.org/us/ok/duncan/profile/electrician/jett-solutions-llc-0995-90021395/#sealclick"
          target="_blank"
          rel="noreferrer"
        >
          <img
            src="http://seal-oklahomacity.bbb.org/seals/gray-seal-150-110-bbb-90021395.png"
            style={{ border: 0 }}
            alt="Jett Solutions, LLC BBB Business Review"
          />
        </a>
      </Div>
      <Div>
        <Image src="../../static/women-owned.png" />
      </Div>
    </Div>
  )
}

export default FooterContent
