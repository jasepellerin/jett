import ContentSection from '../../../components/ContentSection'
import licenses from './licenses.json'

const makeTable = (licenses) => {
  const halfLength = Math.ceil(licenses.length / 2)
  const secondHalf = Array.from(licenses)
  const firstHalf = secondHalf.splice(0, halfLength)
  let currentIndex = 0
  const makeRow = (startIndex) => {
    const getContents = (half, index) => {
      return index < half.length ? <><td>{half[index].state}</td><td>{half[index].number}</td></> : <><td></td><td></td></>
    }
    return (
      <tr key={startIndex}>
        {getContents(firstHalf, startIndex)}
        {getContents(secondHalf, startIndex)}
      </tr>
    )
  }
  const rows = []
  while (currentIndex < firstHalf.length) {
    rows.push(makeRow(currentIndex))
    currentIndex += 1
  }
  return (
    <table>
      {rows }
    </table>
  )
}

const Description = () => (
  <ContentSection
    theme='light'
  >
    <h2>Electrical Licenses by State</h2>
    {makeTable(licenses.electrical)}

    <h2>Alarm Licenses by State</h2>
    {makeTable(licenses.alarm)}
  </ContentSection>
)

export default Description
