import Div from '../../../../components/Base/Div'
import styles from './EmployeeInfo.scss'

const EmployeeInfo = ({ contactInfo, name, offsetX = 0, offsetY = 0, src, title }) => (
  <Div styles={styles}>
    <Div
      className={styles.image}
      style={
        {
          backgroundImage: `url(${src})`,
          backgroundPosition: `${50 + offsetX}% ${50 + offsetY}%`
        }
      }
    />
    <h3 className={styles.name}>{name}</h3>
    <p className={styles.title}>{title}</p>
    <ul>
      {contactInfo.office && <p><b>Office:</b> {contactInfo.office}</p>}
      {contactInfo.fax && <p><b>Fax:</b> {contactInfo.fax}</p>}
      {contactInfo.cell && <p><b>Cell:</b> {contactInfo.cell}</p>}
      {contactInfo.email && <p><a href={`mailto:${contactInfo.email}`}>{contactInfo.email}</a></p>}
    </ul>
  </Div>
)

export default EmployeeInfo
