import ContentSection from '../../../components/ContentSection'
import EmployeeInfo from './EmployeeInfo'
import employeeData from './employees'
import FlexContainer from '../../../components/FlexContainer'

const Description = () => (
  <ContentSection
    theme='dark'
    notchLocation='both'
  >
    <h2>Leadership</h2>
    <FlexContainer variation="wrap">
      {employeeData.map((data) => <EmployeeInfo {...data} key={data.name} />)}
    </FlexContainer>
  </ContentSection>
)

export default Description
