import Splash from '../../Shared/Splash'

const AboutSplash = () => (
  <Splash backgroundImage='/static/about/splash.jpg' notchLocation='bottom-left'>
    <h1>About Us</h1>
    <p>
        Who We Are
    </p>
  </Splash>
)

export default AboutSplash
