import AboutSplash from './Splash'
import Description from './Description'
import Leadership from './Leadership'
import Licenses from './Licenses'

const About = () => (
  <>
    <AboutSplash />
    <Description />
    <Leadership />
    <Licenses />
  </>
)

export default About
