import ContentSection from '../../../components/ContentSection'

const Description = () => (
  <ContentSection
    theme='light'
  >
    <h2>Our Story</h2>
    <p>
      Based in <b>Duncan, OK</b>, Jett Solutions, LLC provides a diverse set of services in electrical, gas and panel solutions amongst many other areas with professionalism, attention to detail, and utmost efficiency.
    </p>
    <p>
      Having worked with companies that are Fortune 500 companies and the leaders in their fields on a daily basis, Jett Solutions has set a benchmark on value and strive to grow with the same principles which made us who we are today.
    </p>
    <p>
      At Jett Solutions we are more than service providers, we are a woman-owned family guided by Christian ethics, integrity, and a passion for creating long-lasting, quality solutions.
    </p>
    <p>
      Electrically Licensed in a majority of the United States Jett has worked locally, nationally and internationally and our vast experience accumulated along the years have led us to a single result every single time – complete client satisfaction.
    </p>
    <p>
      We have passed our work education and experience from generation to generation to ensure a smooth transition and company reputation to ensure our clients can expect the same professionalism and attentive behavior on all projects we handle locally and nationally.
    </p>
    <p>
      Thriving on challenges and providing solutions for each industry need is what we stand by as we look forward to working with you on future projects.
    </p>
  </ContentSection>
)

export default Description
