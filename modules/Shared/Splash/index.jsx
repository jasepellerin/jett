import ContentSection from '../../../components/ContentSection'

const Splash = ({ backgroundImage, children, style, theme, ...rest }) => (
  <ContentSection
    style={{ backgroundImage: `url(${backgroundImage})`, ...style }}
    theme={`${theme} splash`}
    {...rest}
  >
    {children}
  </ContentSection>
)

export default Splash
