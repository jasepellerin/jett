import Cookies from 'js-cookie'
const cookieName = 'jett-login'

const cookieManager = {
  getCookie () {
    return Cookies.get(cookieName)
  },
  setCookie () {
    Cookies.set(cookieName, 'valid', { expires: 14 })
  },
  removeCookie () {
    Cookies.remove(cookieName)
  }
}

export default cookieManager
